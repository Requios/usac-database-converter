package nl.alpenclub.usac;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * Created by Christian on 28-7-2017.
 */
public class MyGUI {
    private JTextArea inputArea;
    private JTextArea outputArea;
    private JButton button;
    private JPanel MyGUI;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (UnsupportedLookAndFeelException e) {
        }

        JFrame frame = new JFrame("USAC Database Converter");
        frame.setContentPane(new MyGUI().MyGUI);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public MyGUI() {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = inputArea.getText();
                String output = Convert.convert(input);
                outputArea.setText(output);
                outputArea.grabFocus();
            }
        });

        outputArea.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                outputArea.selectAll();
            }
        });
    }
}
