package nl.alpenclub.usac;

import java.text.Normalizer;
import java.util.*;

/**
 * Converts Zope database csv into Buddypress Members Import csv.
 * Created by Christian on 28-7-2017.
 *
 * Columns: user_login;Naam;display_name;nickname;Leden ID;Voornaam;Tussenvoegsel;Achternaam;Initialen;Adres;Postcode;Plaats;Land;Telefoon nummer;Adres 2;Postcode 2;Plaats 2;Telefoon nummer 2;Land 2;
 * Telefoon alternatief;Geboortedatum;Geslacht;user_email;Lid vanaf;Lid af;Soort lid;NKBV nummer;Studie;Rekeningnummer;IBAN;BIC;Studentnummer;Klimjaarkaart;
 * Opmerking;Donatiejaar;Onderwijsinstelling vorige;Onderwijsinstelling;Automatische incasso;Olympas;KMI;Rijbewijs;Commissies;Bestuur;
 */
public class Convert {
    public static String convert(String input){
        String[] lines = input.split("\n");
        int emailnumber = 128;
        String emails = "Emails vervangmij"+emailnumber+"@mailinator.com till ";
        System.out.println("Irregular email addresses:");

        String[] fieldsArray = lines[0].split(";", -1);
        List<String> fields = Arrays.asList(fieldsArray);
        lines[0] = "";
        for (int j = 0; j < fields.size(); j++) {
            lines[0] = lines[0].concat("\"" + fields.get(j) + "\"");
            if (j != fields.size() - 1) lines[0] = lines[0].concat(",");
        }

        for (int i = 1; i < lines.length; i++) {
            fieldsArray = lines[i].split(";", -1);
            fields = Arrays.asList(fieldsArray);

            fields.set(0, generateUsername(fields));        //user_login
            String name = generateName(fields);
            fields.set(1, name);                            //Naam
            fields.set(2, name);                            //display_name
            fields.set(3, name);                            //nickname

            fields.set(13, convertPhone(fields.get(13)));   //Telefoonnummer
            fields.set(17, convertPhone(fields.get(17)));   //Telefoonnummer 2
            fields.set(19, convertPhone(fields.get(19)));   //Telefoonnummer alternatief

            fields.set(20, convertDate(fields.get(20)));    //Geboortedatum
            fields.set(23, convertDate(fields.get(23)));    //Lid vanaf
            fields.set(24, convertDate(fields.get(24)));    //Lid af

            String geslacht = fields.get(21);
            switch (geslacht) {
                case "m": geslacht = "Man::"; break;
                case "v": geslacht = "Vrouw::"; break;
            }
            fields.set(21, geslacht);

            String email = fields.get(22);
            //If no email exists, create custom Mailinator emails
            if (email.equals("")){
                email = "vervangmij" + emailnumber + "@mailinator.com";
                emailnumber++;
            }
            //If no @ is present, add '@usac.climbing.nl'
            if (!email.contains("@")) {
                email = email + "@usac.climbing.nl";
            }
            email = email.toLowerCase().trim().replace(' ', '.');
            fields.set(22, email);

            String lid = fields.get(25);
            switch (lid) {
                case "lid":             lid = "Lid::"; break;
                case "erelid":          lid = "Erelid::"; break;
                case "alumnus":         lid = "Alumnus::"; break;
                case "donateur":        lid = "Donateur::"; break;
                case "ouddonateur":     lid = "Oud donateur::"; break;
                case "relatie":         lid = "Relatie::"; break;
                case "overleden":       lid = "Overleden::"; break;
                case "aanmeldingen":    lid = "Aanmeldingen::"; break;
            }
            fields.set(25, lid);

            fields.set(26, removeZeros(fields.get(26)));    //NKBV nummer
            fields.set(28, removeZeros(fields.get(28)));    //Rekeningnummer
            fields.set(32, binaryToJaNee(fields.get(32)));  //Klimjaarkaart

            //Split opmerking into Ervaring and Motivatie
            //Does not work
            String opmerking = fields.get(33);
            String ervaring = "";
            String motivatie = "";
//            String[] splits = opmerking.split("(Ervaring:  |Motivatie:  )");
//            opmerking = splits[0];
//            if (splits.length > 1) ervaring = splits[1];
//            if (splits.length > 2) motivatie = splits[2];
            fields.set(33, opmerking);


            String onderwijsinstelling = fields.get(36);
            switch (onderwijsinstelling) {
                case "onbekend":                onderwijsinstelling = ""; break;
                case "UU":                      onderwijsinstelling = "Universiteit Utrecht::"; break;
                case "UCU":                     onderwijsinstelling = "University College Utrecht::"; break;
                case "HU":                      onderwijsinstelling = "Hogeschool Utrecht::"; break;
                case "overig binnen Utrecht":   onderwijsinstelling = "Anders, binnen Utrecht::"; break;
                case "overig buiten Utrecht":   onderwijsinstelling = "Anders, buiten Utrecht::"; break;
                case "niet studerend":          onderwijsinstelling = "Niet studerend::"; break;
                case "Afgestudeerd":            onderwijsinstelling = "Afgestudeerd::"; break;
            }
            fields.set(36, onderwijsinstelling);

            String incasso = "";
            if (fields.get(37).equals("1"))
                incasso += "Contributie::";
            if (fields.get(38).equals("1"))
                incasso += "Weekenden::";
            if (fields.get(39).equals("1"))
                incasso += "Overige activiteiten::";
            if (incasso.length() > 22) //If more than one, remove trailing ::
                incasso = incasso.substring(0, incasso.length() - 2);
            fields.set(37, incasso);

            fields.set(40, binaryToJaNee(fields.get(40)));  //Olympas
            fields.set(41, convertDate(fields.get(41)));    //Rijbewijs

            fields.set(42, fields.get(42)); //Commissies
            fields.set(43, fields.get(43)); //Bestuur

            //After incasso readings
            fields.set(38, ervaring);
            fields.set(39, motivatie);

            //Concatenate all fields into line
            //Add " around every field and , in between
            lines[i] = "";
            for (int j = 0; j < fields.size(); j++) {
                //Remove accents (while keeping the base letter)
                fields.set(j, Normalizer.normalize(fields.get(j), Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", ""));

                //Replace " sign
                fields.set(j, fields.get(j).replace("\"", ""));

                lines[i] = lines[i].concat("\"" + fields.get(j) + "\"");
                if (j != fields.size() - 1) lines[i] = lines[i].concat(",");
            }

            //Find irregular email addresses according to the RFC 5322 Official Standard
            String mail = fields.get(22);
            if (!mail.matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")){
                System.out.println(mail);
            }
        }

        System.out.println(emails + "vervangmij"+(emailnumber-1)+"@mailinator used.");

        //Concatenate all lines
        String output = "";
        for (String line : lines) {
            output += line + "\n";
        }
        return output;
    }

    /**
     * Generates username in format "voornaam.tussenvoegsel.achternaam"
     * @param fields
     * @return
     */
    private static String generateUsername(List<String> fields){
        String voornaam = fields.get(5).trim();
        String tussenvoegsel = fields.get(6).trim();
        if (!tussenvoegsel.equals("")) tussenvoegsel = "." + tussenvoegsel;
        String achternaam = fields.get(7).trim();
        if (!achternaam.equals("")) achternaam = "." + achternaam;

        String naam = voornaam + tussenvoegsel + achternaam;
        naam = naam.toLowerCase().replace(' ', '.');
        //Remove accents (while keeping the base letter)
        naam = Normalizer.normalize(naam, Normalizer.Form.NFKD);
        naam = naam.replaceAll("[^\\p{ASCII}]", "");
        return naam;
    }

    /**
     * Generates name from Voornaam, Tussenvoegsel and Achternaam with proper spacing
     * @param fields
     * @return
     */
    private static String generateName(List<String> fields){
        String voornaam = fields.get(5).trim();
        String tussenvoegsel = fields.get(6).trim();
        if (!tussenvoegsel.equals("")) tussenvoegsel = " " + tussenvoegsel;
        String achternaam = " " + fields.get(7).trim();

        return voornaam + tussenvoegsel + achternaam;
    }

    /**
     * Some phone numbers are missing the 0 in front.
     * To be on the safe side, only numbers that are 9 long and don't start with a 0 will have an added 0.
     * @param old Phone number to convert
     * @return
     */
    private static String convertPhone(String old){
        if (old.length() == 9 && !old.substring(0,1).equals("0"))
            return "0" + old;
        else
            return old;
    }

    /**
     * Converts date from 'dd-mm-YYYY' to 'YYYY-mm-dd 00:00:00' format
     * @param old
     * @return
     */
    private static String convertDate(String old){
        if (old.equals("") || old.equals("0")) return "";

        String[] parts = old.split("-");
        return parts[2] + "-" + parts[1] + "-" + parts[0] + " 00:00:00";
    }

    /**
     * Removes entries that contain only a '0'.
     * @param old
     * @return
     */
    private static String removeZeros(String old){
        if (old.equals("0")) return "";
        else return old;
    }

    /**
     * Converts '0' to 'Nee::' and '1' to 'Ja::'
     * @param binary
     * @return
     */
    private static String binaryToJaNee(String binary){
        switch (binary) {
            case "0": binary = "Nee::"; break;
            case "1": binary = "Ja::"; break;
        }
        return binary;
    }
}
